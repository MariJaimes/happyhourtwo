package com.example.navigationbottomexample;

public class RestaurantInfo {

    private String Descripion;

    public RestaurantInfo(String descripion) {
        Descripion = descripion;
    }

    public RestaurantInfo(){

    }


    public String getDescripion() {
        return Descripion;
    }

    public void setDescripion(String descripion) {
        Descripion = descripion;
    }
}
