package com.example.navigationbottomexample;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;

//import com.android.volley.RequestQueue;
//import com.android.volley.toolbox.Volley;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private BottomNavigationView mMainNav;
    private Toolbar toolbar;
    private FrameLayout mMainFrame;
    private Dialog myDialog;
    private MyRecyclerViewAdapter listAdapter;
    private MenuItem itemSearch;
    private   EventFragment fragmentName;
    private     ArrayList<Event> transactionList;
    private int check;
    private int cantStars,cantPrice;
    private EditText txtStars,txtPrice;

    private EventFragment eventFragment;
    private MapFragment mapFragment;
    private SearchFragment searchFragment;
    private HappyHourFragment happyHourFragment;
    private RestaurantProfile restaurantProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDialog = new Dialog(this);

//        RequestQueue queue = Volley.newRequestQueue(this);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference().child("Event");

        // Attach a listener to read the data at our posts reference
         ref.addValueEventListener(new ValueEventListener() {
            @Override
             public void onDataChange(DataSnapshot dataSnapshot) {
                 Object post = dataSnapshot.getValue(Object.class);
                 System.out.println(post);
             }
            @Override
             public void onCancelled(DatabaseError databaseError)
             {
                System.out.println("The read failed: " + databaseError.getCode());
             }
         });

        mMainFrame = (FrameLayout) findViewById(R.id.main_frame);
        mMainNav = (BottomNavigationView) findViewById(R.id.main_nav);
        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        getSupportActionBar().hide();
        mapFragment  = new MapFragment();
        eventFragment = new EventFragment();
        searchFragment = new SearchFragment();
        happyHourFragment = new HappyHourFragment();
        restaurantProfile = new RestaurantProfile();



        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){

                    case R.id.nav_map :
                        ((EventFragment) eventFragment).listEvents = null;
                        ((HappyHourFragment) happyHourFragment).listHappyHours = null;
                        getSupportActionBar().hide();
                        setFragment(mapFragment);
                        return true;

                    case R.id.nav_hours :
                        ((EventFragment) eventFragment).listEvents = null;
                        ((HappyHourFragment) happyHourFragment).listHappyHours = null;
                        setFragment(happyHourFragment);
                        getSupportActionBar().show();
                        return true;

                    case R.id.nav_events :
                        ((EventFragment) eventFragment).listEvents = null;
                        ((HappyHourFragment) happyHourFragment).listHappyHours = null;
                        getSupportActionBar().show();
                        setFragment(eventFragment);
                        return true;


                        default:
                            return false;

                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.nav_search:
                TextView txtClose;
                Button btnClose,btnApply;
                myDialog.setContentView(R.layout.custompopup);
                btnClose = (Button) myDialog.findViewById(R.id.btnClose);
                txtClose = (TextView) myDialog.findViewById(R.id.txtClose);
                btnApply = (Button) myDialog.findViewById(R.id.idbtn_apply);
                btnApply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        txtStars = (EditText) myDialog.findViewById(R.id.textStars);
                        txtPrice = (EditText) myDialog.findViewById(R.id.textPrecio);

                       if(check==1){

                        }else if(check==2){

                            if(eventFragment.listEvents !=null){
                                eventFragment.filtrarLista(txtStars.getText().toString());

                            }else if (happyHourFragment.listHappyHours != null){
                                happyHourFragment.filtrarStars(txtStars.getText().toString());
                            }

                        }else if(check==3){
                           String canDigitos;
                           canDigitos = "$$$";
                           if(txtPrice.getText().toString().length() <= 4){
                               canDigitos = "$";
                           }else if(txtPrice.getText().toString().length() == 5){
                               canDigitos = "$$";
                           }else {
                               canDigitos = "$$$";
                           }
                           if(eventFragment.listEvents !=null){
                               eventFragment.filtrarByPrice(canDigitos);

                           }else if (happyHourFragment.listHappyHours != null){
                               happyHourFragment.filtrarByPrice(canDigitos);
                           }
                        }


                        myDialog.dismiss();
                    }
                });
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });
                txtClose.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                //myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
                myDialog.show();
        }
        return true;
    }


    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.top_items,menu);
        return true;
    }


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        RadioButton btn1,btn2,btn5;
        cantStars=0;
        cantPrice=0;

        btn2 = (RadioButton) myDialog.findViewById(R.id.radioButton4);
//        btn3 = (RadioButton) myDialog.findViewById(R.id.radioButton5);
//        btn4 = (RadioButton) myDialog.findViewById(R.id.radioButton6);
        btn5 = (RadioButton) myDialog.findViewById(R.id.radioButton7);
        txtPrice = (EditText) myDialog.findViewById(R.id.textPrecio);
        txtStars = (EditText) myDialog.findViewById(R.id.textStars);
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButton4:
                if (checked)
                    check = 2;
//                    btn3.setVisibility(View.INVISIBLE);
//                    btn4.setVisibility(View.INVISIBLE);
                    btn5.setVisibility(View.INVISIBLE);
                    txtStars.setHint("Cantidad de estrellas");

//                    happyHourFragment.llenarByStars();
//                    eventFragment.llenarLista();
                    txtStars.setVisibility(View.VISIBLE);
                    break;
            case R.id.radioButton7:
                if (checked)
                    check = 3;
//                    btn3.setVisibility(View.INVISIBLE);
//                    btn4.setVisibility(View.INVISIBLE);
                    btn2.setVisibility(View.INVISIBLE);
                    txtPrice.setHint("Digite el monto");
                    txtPrice.setVisibility(View.VISIBLE);
                    break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setFragment(mapFragment);
    }


    private void setFragment(Fragment fragment){
        ((EventFragment) eventFragment).listEvents = null;
        ((HappyHourFragment) happyHourFragment).listHappyHours = null;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame,fragment);
        fragmentTransaction.commit();
    }
}
