package com.example.navigationbottomexample;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends Fragment implements View.OnClickListener {


    RecyclerView recyclerInfo;
    public  ArrayList<Event> listEvents;
    public Fragment eventFragment;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref ;
    DatabaseReference refRest ;
    long maxIdEvent;
    Event event;
    HappyHourFragment happyHourFragment;
    MyRecyclerViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_display_events,container,false);

        eventFragment = new EventFragment();
        happyHourFragment = new HappyHourFragment();
        happyHourFragment.listHappyHours=null;

        listEvents = null;

        listEvents = new ArrayList<Event>();

        recyclerInfo = view.findViewById(R.id.myrecyview);
        recyclerInfo.setLayoutManager(new LinearLayoutManager(getActivity()));
        //crearEvento();

        llenarLista();
        adapter = new MyRecyclerViewAdapter(listEvents);
        adapter.setOnClickListener(this);

        recyclerInfo.setAdapter(adapter);

        return view;

    }

    @Override
    public void onClick(View v) {

        RestaurantProfile restaurantProfile = new RestaurantProfile();

        String idRestaurante;
        idRestaurante =listEvents.get(recyclerInfo.getChildAdapterPosition(v)).getIdRestaurante();
        irAlRestaurant(restaurantProfile,idRestaurante);

        }
    @SuppressLint("ResourceType")
    public void irAlRestaurant(Fragment restaurantProfile, String idRestaurante) {


        Bundle mibundle = new Bundle();
        mibundle.putString("idRestaurante",idRestaurante);

        restaurantProfile.setArguments(mibundle);
        FragmentManager fragmentManager = getFragmentManager();
        //Replace intent with Bundle and put it in the transaction
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.main_frame,restaurantProfile);
        fragmentTransaction.commit();
    }

    public void crearEvento(){
        event = new Event();
        event.setDateStart("02/04/2019");
        event.setDateEnd("28/05/2019");
        event.setDescription("Comida en 2mil");
        event.setPrecio("$");
        event.setEstrellas("2");
        event.setTipo("E");
        event.setTiempo("De 1 pm a 3 pm");
        event.setIdRestaurante("1");
        event.setIdEvent("1");
        event.setImg("...");
        event.setNombreRest("Buena vida");
        ref = database.getReference().child("Event");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                maxIdEvent =  (dataSnapshot.getChildrenCount());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        ref.child(String.valueOf(maxIdEvent+1)).setValue(event);

    }


    public  void llenarLista(){
        ref = database.getReference().child("Event");
        Query var = ref.orderByChild("tipo").equalTo("E");

//        refRest = database.getReference().child("Restaurant");
//        Query varRest = ref.orderByChild("idRestaurante").equalTo("");

        var.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (listEvents!=null){
                    listEvents.removeAll(listEvents);
                }

                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Event eventos = snapshot.getValue(Event.class);
                    listEvents.add(eventos);
                    //listEvents.add(new Event(eventos.getNombreRest(),eventos.getDateEnd(),eventos.getDateStart(),eventos.getDescription(),eventos.getPrecio(),eventos.getEstrellas()));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        System.out.println(listEvents);
    }


    public void filtrarLista(String filtro){

        if (listEvents  != null ){
            ArrayList<Event> nuevaLista = new ArrayList<>();

            for(Event e :listEvents)
            {
                if (e.getEstrellas().toLowerCase().contains(filtro)){
                    nuevaLista.add(e);
                }
            }
            adapter.actualizarLista(nuevaLista);
        }
    }
    public void filtrarByPrice(String filtro){

        if (listEvents  != null ){
            ArrayList<Event> nuevaLista = new ArrayList<>();

            for(Event e :listEvents)
            {
                if (e.getPrecio().toLowerCase().contains(filtro)){
                    nuevaLista.add(e);
                }
            }
            adapter.actualizarLista(nuevaLista);
        }
    }



//    @SuppressLint("ResourceType")
//    public void irAlRestaurant(Fragment mapFragment) {
//
//
//        Bundle mibundle = new Bundle();
//        mibundle.putString("idRestaurante",listEvents);
//        mibundle.putDoubleArray("coordenadas",arrayDouble);
//        mibundle.putBoolean("irAlMapa",true);
//
//        mapFragment.setArguments(mibundle);
//        FragmentManager fragmentManager = getFragmentManager();
//        //Replace intent with Bundle and put it in the transaction
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//
//        fragmentTransaction.replace(R.id.main_frame,mapFragment);
//        fragmentTransaction.commit();
//    }
}
