package com.example.navigationbottomexample;

import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder>
    implements View.OnClickListener {


    public ArrayList<Event> aData;
    private View.OnClickListener listener;


    public MyRecyclerViewAdapter(final ArrayList aData) {

        this.aData = aData;

    }
    public void setOnClickListener(View.OnClickListener listenerM){
        this.listener = listenerM;

    }
    @Override
    public void onClick(View v) {
        if (listener!=null){
            listener.onClick(v);

        }

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView myTextName = null;
        TextView myTextDesc = null;
        TextView myTextDateStar = null;
        TextView myTextDateEnd = null;
        TextView myTextPrice = null;
        RatingBar myRatingBar = null;

        public ViewHolder(View itemview){
            super(itemview);
            myTextName = itemview.findViewById(R.id.idNombre);
            myTextDesc = itemview.findViewById(R.id.idDesc);
            myTextDesc.setMovementMethod(new ScrollingMovementMethod());
            myTextPrice = itemview.findViewById(R.id.idPrecio);
            myTextDateStar = itemview.findViewById(R.id.idStar);
            myTextDateEnd = itemview.findViewById(R.id.idFin);
            myRatingBar = itemview.findViewById(R.id.idStars);
        }

    }

    @Override
    public MyRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recy_row,null, false);

        view.setOnClickListener(this);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        Event dataText = aData.get(position).getNombre();
        holder.myTextName.setText(aData.get(position).getNombreRest());
        holder.myTextDesc.setText(aData.get(position).getDescription());
        holder.myTextPrice.setText(aData.get(position).getPrecio());
        holder.myTextDateStar.setText(aData.get(position).getDateStart()+"-");
        holder.myTextDateEnd.setText(aData.get(position).getDateEnd());
        holder.myRatingBar.setRating(Float.parseFloat(aData.get(position).getEstrellas()));
    }

    @Override
    public int getItemCount(){
        return aData.size();
    }

    public void actualizarLista(ArrayList<Event> nuevaLista){
        aData = new ArrayList<>();
        aData.addAll(nuevaLista);
        notifyDataSetChanged();
    }

}
