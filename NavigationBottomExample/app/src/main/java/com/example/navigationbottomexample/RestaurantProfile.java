package com.example.navigationbottomexample;


import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantProfile extends Fragment implements View.OnClickListener {

    RecyclerView restaurantInfo;
    public ArrayList<RestaurantInfo> descriptions;
    public ArrayList<Comentarios> listComentarios;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref ;
    EditText comentariosInput;
    long maxIdComentarios, maxIdRestaurante, idRestauranteActual;
    Comentarios comentarios;
    TextView descripcion, nombreRest;
    String desc,nombreR;
    Restaurant  restaurants;
    String latitud, longitud;
    MyCommentsAdapter adapter;
    String idRest;


    public RestaurantProfile() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       // R.id.idImagen.setCompoundDrawablesWithIntrinsicBounds( R.drawable.gps, 0, 0, 0);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_restaurant_profile, container, false);
       descripcion = (TextView)view.findViewById(R.id.idDescripcion);
        nombreRest = (TextView)view.findViewById(R.id.idRestaurante);
        //crearRestaurante();
        if (getArguments()!=null){

            idRest = getArguments().getString("idRestaurante");
        }
        obtenerRestaurant();

        llenarDesc();

        comentarios = new Comentarios();

        comentariosInput = view.findViewById(R.id.idComentario);
        listComentarios = null;
        Button irAlMapa = (Button) view.findViewById(R.id.idUbicacion);
        Button agregar = (Button) view.findViewById(R.id.idAgregar);
        irAlMapa.setOnClickListener(this);
        agregar.setOnClickListener(this);

        listComentarios= new ArrayList<Comentarios>();

        restaurantInfo = view.findViewById(R.id.myrecyviewComments);
        restaurantInfo.setLayoutManager(new LinearLayoutManager(getActivity()));


        adapter = new MyCommentsAdapter(listComentarios);
        restaurantInfo.setAdapter(adapter);


        return view;
    }



    private void llenarDesc() {

        ref = database.getReference().child("Comentarios");
        Query var = ref.orderByChild("idRestaurante").equalTo(idRest);
        var.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (listComentarios!=null){
                    listComentarios.removeAll(listComentarios);
                }


                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Comentarios comentariosRest = snapshot.getValue(Comentarios.class);
                    listComentarios.add(comentariosRest);

                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    public void obtenerRestaurant(){

        ref = database.getReference().child("Restaurant").child(idRest);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                desc = dataSnapshot.child("descripcion").getValue().toString();
                nombreR = dataSnapshot.child("nombreRestaurant").getValue().toString();
                latitud = dataSnapshot.child("latitud").getValue().toString();
                longitud = dataSnapshot.child("longitud").getValue().toString();
                descripcion.setText(desc);
                nombreRest.setText(nombreR);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
//        System.out.println(idRest);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.idUbicacion:


                MapFragment mapFragment = new MapFragment();
                irAlMapa(mapFragment);

                break;
            case R.id.idAgregar:
                String comentariosInputDetalle = comentariosInput.getText().toString();

                System.out.println(idRestauranteActual);
                comentarios.setDetalles(comentariosInputDetalle);
                ref = database.getReference().child("Comentarios");

                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            maxIdComentarios = (dataSnapshot.getChildrenCount());
                            System.out.println(maxIdComentarios);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {


                    }
                });


                String strLong = Long.toString(maxIdComentarios+1);
                comentarios.setIdComment(strLong);
                comentarios.setIdRestaurante(idRest);

                ref.child(String.valueOf(maxIdComentarios +1)).setValue(comentarios);
                //ref.push().setValue(guardarComentarios);
                Toast.makeText(getActivity(), "Comentario ingresado", Toast.LENGTH_LONG ).show();
                limpiarCampos();
                break;
        }
    }

    public void limpiarCampos(){
        if (comentariosInput.getText().toString().isEmpty()){

        }else{
            comentariosInput.setText("");
        }
    }

    public void crearRestaurante(){
        ref = database.getReference().child("Restaurant");

        restaurants = new Restaurant();
        restaurants.setIdRestaurant("3");
        restaurants.setLatitud("9.94727");
        restaurants.setLongitud("-84.50626");
        restaurants.setNombreRestaurant("Mcdonalds");
        restaurants.setDescripcion("Comida rapida de hamburguesas");
        ref = database.getReference().child("Restaurant");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                maxIdRestaurante =  (dataSnapshot.getChildrenCount());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        ref.child(String.valueOf(maxIdRestaurante+1)).setValue(restaurants);

    }

    @SuppressLint("ResourceType")
    public void irAlMapa(Fragment mapFragment) {


        Bundle mibundle = new Bundle();
        double[ ] arrayDouble = new double[2];
        double lat = Double.parseDouble(latitud);
        double longi = Double.parseDouble(longitud);
        arrayDouble[0] = lat;
        arrayDouble[1] = longi;
        mibundle.putString("nombreRestaurante",nombreR);
        mibundle.putDoubleArray("coordenadas",arrayDouble);
        mibundle.putBoolean("irAlMapa",true);

        mapFragment.setArguments(mibundle);
        FragmentManager fragmentManager = getFragmentManager();
        //Replace intent with Bundle and put it in the transaction
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.main_frame,mapFragment);
        fragmentTransaction.commit();
        }


}
