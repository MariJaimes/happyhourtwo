package com.example.navigationbottomexample;

import java.io.Serializable;

public class Event implements Serializable {

    private String dateEnd;
    private String dateStart;
    private String description;
    private String precio;
    private String estrellas;
    private Restaurant Restaurant;
    private String tipo;
    private String img;
    private String tiempo;
    private String idEvent;
    private String idRestaurante;
    private String nombreRest;


    public Event(){

    }


    public Event(String nombreRest, String dateEnd, String dateStart, String description, String precio, String estrellas) {
        this.nombreRest = nombreRest;
        this.dateEnd = dateEnd;
        this.dateStart = dateStart;
        this.description = description;
        this.precio = precio;
        this.estrellas = estrellas;
    }

    public Event(String dateEnd, String dateStart, String description, String precio, String estrellas, Restaurant Restaurant, String tipo, String img, String tiempo, String idEvent) {
        this.dateEnd = dateEnd;
        this.dateStart = dateStart;
        this.description = description;
        this.precio = precio;
        this.estrellas = estrellas;
        this.Restaurant = Restaurant;
        this.tipo = tipo;
        this.img = img;
        this.tiempo = tiempo;
        this.idEvent = idEvent;
    }
    public String getNombreRest() {
        return nombreRest;
    }

    public void setNombreRest(String nombreRest) {
        this.nombreRest = nombreRest;
    }
    public String getIdRestaurante() {
        return idRestaurante;
    }

    public void setIdRestaurante(String idRestaurante) {
        this.idRestaurante = idRestaurante;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getEstrellas() {
        return estrellas;
    }

    public void setEstrellas(String estrellas) {
        this.estrellas = estrellas;
    }

    public Restaurant getRestaurant() {
        return Restaurant;
    }

    public void setRestaurant(Restaurant Restaurant) {
        this.Restaurant = Restaurant;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(String idEvent) {
        this.idEvent = idEvent;
    }
}
