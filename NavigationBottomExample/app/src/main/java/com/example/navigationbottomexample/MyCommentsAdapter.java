package com.example.navigationbottomexample;

import android.app.AlertDialog;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class MyCommentsAdapter extends RecyclerView.Adapter<MyCommentsAdapter.ViewHolder> {

    public ArrayList<Comentarios> dataDescriptions;
//    private LayoutInflater ainflater;

    public MyCommentsAdapter(ArrayList<Comentarios> dataDescriptions){
        this.dataDescriptions = dataDescriptions;
    }
    public MyCommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comments_rows,null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.myTextDescription.setText(dataDescriptions.get(i).getDetalles());

    }

    @Override
    public int getItemCount() {
        return dataDescriptions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView myTextDescription = null;

        public ViewHolder(View itemView) {
            super(itemView);
            myTextDescription = itemView.findViewById(R.id.idDescComments);
            myTextDescription.setMovementMethod(new ScrollingMovementMethod());
        }
    }
}
