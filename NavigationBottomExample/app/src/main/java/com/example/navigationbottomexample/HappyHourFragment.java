package com.example.navigationbottomexample;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HappyHourFragment extends Fragment implements View.OnClickListener{

    RecyclerView recyclerInfo;
    public ArrayList<Event> listHappyHours;
    public Fragment happyHour;
    EventFragment eventFragment;
    MyRecyclerViewAdapter adapter;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_display_events,container,false);

        happyHour = new HappyHourFragment();
        eventFragment = new EventFragment();
        eventFragment.listEvents = null;

        listHappyHours = null;

        listHappyHours = new ArrayList<>();


        recyclerInfo = view.findViewById(R.id.myrecyview);
        recyclerInfo.setLayoutManager(new LinearLayoutManager(getActivity()));


        llenarLista();
        adapter = new MyRecyclerViewAdapter(listHappyHours);
        adapter.setOnClickListener(this);
        recyclerInfo.setAdapter(adapter);

        return view;

    }
    public  void llenarLista(){
        ref = database.getReference().child("Event");
        Query var = ref.orderByChild("tipo").equalTo("H");
        var.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (listHappyHours!=null){

                    listHappyHours.removeAll(listHappyHours);
                }

                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Event eventos = snapshot.getValue(Event.class);
                    listHappyHours.add(eventos);
                    //listEvents.add(new Event(eventos.getNombreRest(),eventos.getDateEnd(),eventos.getDateStart(),eventos.getDescription(),eventos.getPrecio(),eventos.getEstrellas()));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        System.out.println(listHappyHours);
    }

    public void filtrarStars(String filtro){
        if (listHappyHours  != null ){
            ArrayList<Event> nuevaLista = new ArrayList<>();

            for(Event e :listHappyHours)
            {
                if (e.getEstrellas().toLowerCase().contains(filtro)){
                    nuevaLista.add(e);
                }
            }
            adapter.actualizarLista(nuevaLista);
        }
    }
    public void filtrarByPrice(String filtro){

        if (listHappyHours  != null ){
            ArrayList<Event> nuevaLista = new ArrayList<>();

            for(Event e :listHappyHours)
            {
                if (e.getPrecio().toLowerCase().contains(filtro)){
                    nuevaLista.add(e);
                }
            }
            adapter.actualizarLista(nuevaLista);
        }
    }

    @Override
    public void onClick(View v) {
        RestaurantProfile restaurantProfile = new RestaurantProfile();

        String idRestaurante;
        idRestaurante =listHappyHours.get(recyclerInfo.getChildAdapterPosition(v)).getIdRestaurante();
        irAlRestaurant(restaurantProfile,idRestaurante);

    }
    @SuppressLint("ResourceType")
    public void irAlRestaurant(Fragment restaurantProfile, String idRestaurante) {


        Bundle mibundle = new Bundle();
        mibundle.putString("idRestaurante",idRestaurante);

        restaurantProfile.setArguments(mibundle);
        FragmentManager fragmentManager = getFragmentManager();
        //Replace intent with Bundle and put it in the transaction
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.main_frame,restaurantProfile);
        fragmentTransaction.commit();
    }
}
