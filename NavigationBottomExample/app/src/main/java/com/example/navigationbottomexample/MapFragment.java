package com.example.navigationbottomexample;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback {
    private LocationManager locManager;
    private Location loc;

    public MapFragment() {
        // Required empty public constructor

    }

    private GoogleMap mMap;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref ;
    public ArrayList<Restaurant> listRestaurantes;
    boolean desdeElRest = false;
    double lat, longi;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (getArguments()!=null){

            String nombre = getArguments().getString("nombreRestaurante");

            double[ ] arrayDouble = new double[2];
            arrayDouble = getArguments().getDoubleArray("coordenadas");
            desdeElRest= getArguments().getBoolean("irAlMapa");
            LatLng sydney = new LatLng(arrayDouble[0], arrayDouble[1]);
            mMap.addMarker(new MarkerOptions().position(sydney).title(nombre));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));

        }


        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        loc = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        lat = loc.getLatitude();
        longi = loc.getLongitude();
        if (desdeElRest==false){
            listRestaurantes= new ArrayList<Restaurant>();
            mostrarRestaurantes();

        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);



    }

    public void mostrarRestaurantes(){
        //final Restaurant restaurants;
        ref = database.getReference().child("Restaurant");
       // restaurants = new Restaurant();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (listRestaurantes!=null){
                    listRestaurantes.removeAll(listRestaurantes);
                }

                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Restaurant restaurant = snapshot.getValue(Restaurant.class);
                    listRestaurantes.add(restaurant);
                    for (Restaurant rest: listRestaurantes) {
                        double lat = Double.parseDouble(rest.getLatitud());
                        double longi = Double.parseDouble(rest.getLongitud());
                        LatLng listaDeRestaurantes = new LatLng(lat, longi);
                        mMap.addMarker(new MarkerOptions()
                        .position(listaDeRestaurantes)
                        .title(rest.getNombreRestaurant())
                        .snippet(rest.getDescripcion()));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(listaDeRestaurantes));
                    }


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        System.out.println(listRestaurantes);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length == 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    LatLng sydney = new LatLng(9.9333296, -84.0833282);
                    mMap.addMarker(new MarkerOptions().position(sydney).title("San José"));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
                    mMap.setMyLocationEnabled(true);

                } else {
                    Toast.makeText(getActivity(), "No se va a mostrar la ubicación actual, por favor activar los permisos.", Toast.LENGTH_LONG ).show();
                    LatLng sydney = new LatLng(9.9333296, -84.0833282);
                    mMap.addMarker(new MarkerOptions().position(sydney).title("San José"));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
                }

            }
        }
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(getActivity(), "Mi ubicación", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMyLocationButtonClick() {


        Toast.makeText(getActivity(), "Ir a mi ubicación", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }
}
