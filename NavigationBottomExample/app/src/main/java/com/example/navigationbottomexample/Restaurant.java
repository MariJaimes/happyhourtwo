package com.example.navigationbottomexample;

import java.io.Serializable;

public class Restaurant implements Serializable {

    public String latitud;
    public String longitud;
    public String idRestaurant;
    public String nombreRestaurant;
    public String descripcion;

    public Restaurant (){

    }


    public Restaurant(String latitud, String longitud, String idRestaurant, String nombreRestaurant,String descripcion) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.idRestaurant = idRestaurant;
        this.nombreRestaurant = nombreRestaurant;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getIdRestaurant() {
        return idRestaurant;
    }

    public void setIdRestaurant(String idRestaurant) {
        this.idRestaurant = idRestaurant;
    }

    public String getNombreRestaurant() {
        return nombreRestaurant;
    }

    public void setNombreRestaurant(String nombreRestaurant) {
        this.nombreRestaurant = nombreRestaurant;
    }
}
