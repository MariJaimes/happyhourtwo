package com.example.navigationbottomexample;

import java.io.Serializable;

public class Comentarios implements Serializable {
    private String Detalles;
    private String IdComment;
    private Restaurant objRestaurant;
    private String IdRestaurante;





    public Comentarios(String detalles, String idComment, Restaurant objRest, String idRestaurante) {
        Detalles = detalles;
        IdComment = idComment;
        this.objRestaurant = objRest;
        IdRestaurante = idRestaurante;
    }

    public Comentarios(String detalles, String idComment, String idRestaurante) {
        Detalles = detalles;
        IdComment = idComment;
    }

    public Comentarios() {
    }
    public String getIdRestaurante() {
        return IdRestaurante;
    }

    public void setIdRestaurante(String idRestaurante) {
        IdRestaurante = idRestaurante;
    }

    public String getDetalles() {
        return Detalles;
    }

    public void setDetalles(String detalles) {
        Detalles = detalles;
    }

    public String getIdComment() {
        return IdComment;
    }

    public void setIdComment(String idComment) {
        IdComment = idComment;
    }

    public Restaurant getObjRest() {
        return objRestaurant;
    }

    public void setObjRest(Restaurant objRest) {
        this.objRestaurant = objRest;
    }
}
